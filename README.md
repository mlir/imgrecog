MLIR/Imgrecog
===

An image recognition application using machine learning principles, with a web interface.

## Installing
	
After getting the files (either by using `git clone` or downloading a zip):

### Debian-based linux:

    $ apt-get install python3 pip python3-pip python3-dev
    $ pip install flask
    $ pip install pillow
    $ pip install scipy
    $ cd imgrecog
    $ ./main.py

### Windows:
	
Install python3 using the installer package. Include scripts and make sure to add python to your %PATH%.

	C:\> easy_install pip
	C:\> pip install flask
	C:\> pip install pillow
	C:\> pip install scipy
	C:\> cd imgrecog
	C:\imgrecog> python main.py

##Running
After the installation is done, open a web browser to `http://localhost:5000/` and if that doesn't work try `http://localhost/`.