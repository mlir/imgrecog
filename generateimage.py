from PIL import Image, ImageFont, ImageDraw

default_size = (512, 512)
default_mode = '1' # '1' monochrome, 'L' greyscale, 'RGB' RGB
default_font = 'LiberationSans-Regular.ttf'

def makeBase(mode=default_mode, size=default_size, color='white'):
    return Image.new(mode, size, color)

def generateImage(translation=(0,0), rotation=0.0, scaling=(1.0, 1.0),
                  glyph='A', font=default_font, fontcolor='black', base=None):
    if not base:
        base = Image.new(default_mode, default_size, 'white')
    # Draw.
    font = ImageFont.truetype(default_font, 256)
    textlayer = Image.new(default_mode, font.getsize(glyph))
    drawing = ImageDraw.Draw(textlayer)
    drawing.text((0,0), glyph, font=font, fill=fontcolor)
    if default_mode == 'RGB':
        mask_mode = 'L'
    else:
        mask_mode = default_mode
    mask = Image.new(mask_mode, textlayer.size)
    drawing = ImageDraw.Draw(mask)
    drawing.text((0,0), glyph, font=font, fill='white')
    # Scale.
    if isinstance(scaling, int) or isinstance(scaling, float):
        scaling = (scaling, scaling)
    if scaling != (1, 1):
        new_size = (round(scaling[0]*textlayer.size[0]),
                    round(scaling[1]*textlayer.size[1]))
        textlayer = textlayer.resize(new_size, resample=Image.BILINEAR)
        mask = mask.resize(new_size, resample=Image.BILINEAR)    
    # Rotate.
    if rotation != 0:
        textlayer = textlayer.rotate(rotation, resample=Image.BILINEAR, expand=1)
        mask = mask.rotate(rotation, resample=Image.BILINEAR, expand=1)
    # Crop.
    bbox = textlayer.getbbox()
    textlayer = textlayer.crop(bbox)
    mask = mask.crop(bbox)
    # Normal size.
    new_size = (round(0.1*textlayer.size[0]),
                round(0.1*textlayer.size[1]))
    textlayer = textlayer.resize(new_size, resample=Image.BILINEAR)
    mask = mask.resize(new_size, resample=Image.BILINEAR)  
    # Translate and paste.
    if translation[0] < 0:
        translation = (0, translation[1])
    elif translation[0] + textlayer.size[0] > base.size[0]:
        translation = (base.size[0] - textlayer.size[0], translation[1])
    if translation[1] < 0:
        translation = (translation[0], 0)
    elif translation[1] + textlayer.size[1] > base.size[1]:
        translation = (translation[0], base.size[1] - textlayer.size[1])
    base.paste(textlayer, translation, mask)
    return base



if __name__ == '__main__':
    #testimage = Image.open('/tmp/test.jpg')
    #if testimage.size != default_size:
    #    testimage = testimage.resize(default_size, resample=Image.BILINEAR)
    im = GenerateImage(rotation=33.0)
    im = GenerateImage(base=im,
                       glyph='G',
                       rotation=190,
                       translation=(40, 30),
                       scaling=2, fontcolor='black')
    im = GenerateImage(base=im,
                       glyph='B',
                       rotation=-12.5,
                       translation=(256, 180),
                       scaling=(5, 10))
    im.show()



    test = im.convert('1').tobytes()
    im2 = Image.frombytes('1', im.size, test)
    im2.show()
