#!/usr/bin/python3

from flask import Flask, render_template, abort, json, request, url_for, redirect
from werkzeug import secure_filename
import os,random
import msc_rec
UPLOAD_DIR = "static/img/upload/"
ALLOWED_EXTENSIONS = set(['png'])
app = Flask(__name__)
app.config['UPLOAD_DIR'] = UPLOAD_DIR


from generateimage import generateImage, makeBase
import utils, numpy

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/about')
def aboutpage():
	return render_template('about.html')

@app.route('/generate',methods=["GET","POST"])
def generate():
	if request.method == "GET":
		return render_template("generate.html")
	size = ( int(request.form['resx']), int(request.form['resy']) )
	translation = ( int(request.form['transx']), int(request.form['transy']) )
	rotation = float(request.form['rot'])
	glyph = request.form['glyph']
	image = generateImage(base = makeBase(size=size),
                              translation = translation,
                              rotation = rotation,
                              glyph = glyph,
                              scaling = 4)
	iid = utils.save_image(image)
	return json.dumps({'status': 'OK','imageid': iid+".png"})

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/recognize', methods=['GET','POST'])
def match_template():
	if request.method == 'POST':
		input_file = request.files['input_file']
		k = float(request.form['k'])
		if input_file and allowed_file(input_file.filename):
			input_filename = secure_filename(utils.makeRandomId(8) + input_file.filename)
			input_file.save(os.path.join(app.config['UPLOAD_DIR'], input_filename))
			input_url = os.path.join(app.config['UPLOAD_DIR'], input_filename)
			output_pics_urls = [os.path.join(app.config['UPLOAD_DIR'], utils.makeRandomId(16)+".png") for _ in range(32)]
			output = msc_rec.msc_recog_test_no_template(input_url,output_pics_urls[:],k=k)
			output_pics_urls.reverse()
			return render_template(
				'result.html',
				output=output,
				template="",
				input=input_url,
				output_pics=output_pics_urls)
	return render_template('rot_recog.html')

if __name__ == '__main__':
	app.run(
		debug=True,
		host="0.0.0.0",
	)
