"""
This is a suggestion for how transformations could be represented.
The solution is a sequence of transformations, as identified by
the final non-zero g-values. Those transformations must be applied
to the input in order to recover the matching part of the image.
A meaningful string representation (such as "45 deg clockwise")
would also make it possible to verify a solution without having
to produce the image.
"""
class Xform:

  def __init__(self, func, inverse, fstr, istr):
    self.func = func
    self.inverse = inverse
    self.fstr = fstr
    self.istr = istr

  def __apply__(self, x):
    return self.func(x)

  def __neg__(self):
    return Xform(self.inverse, self.func, self.istr, self.fstr)

  def __str__(self):
    return self.fstr

  def __repr__(self):
    return self.fstr

# The domain doesn't have to be vectors.
#
# f = lambda v: np.some_vector_operation(v, arg1, arg2)
# i = lambda v: np.inverse_operation(v, arg1, arg2)
# x = Xform(f, i, "vector op", "inverse op")
# >>> -x
# >>> "inverse op"
# >>> x(arg)
# >>> <transformed arg>
