#!/bin/python3
from scipy import misc
import numpy as np
import utils

class TransformContainer:
	def __init__(self, function_reference, args, name="Transform"):
		self.f = function_reference
		self.args = args
		self.name=name
	def __call__(self, arg):
		if type(self.args) is list or type(self.args) is tuple:
			return self.f(arg, *self.args)
		return self.f(arg, self.args)
	def __repr__(self):
		return "%s: %s"%(self.name, self.args)

# Superposition of all transformed images (tr), weighted by gs. copied from msc.py
def superpos(gs, tr):
	return sum([g*u for (g,u) in zip(gs, tr)])

def sum2d(l):
	return sum(sum(l))

def get_matching_template(f ,bs):
	return [
		(sum2d(f*b)/(sum2d(b)-40*max([max(fb) for fb in (f*b)]))) # Return found matching divided by possible matching. -max becaus more pixels matched should be better than higher amount of matches
		for b in bs # For each transformed template/superposition
	] # This one will be used in weigh_matching, raw, thus not neccesary to convert to np array.
# Calculates q for each transformed b
def get_matching(f ,bs):
	return [#Prevent division by zero
		0 if sum2d(b)==0 else (sum2d(f*b)/sum2d(b)) # Return found matching divided by possible matching. -max becaus more pixels matched should be better than higher amount of matches
		for b in bs # For each transformed template/superposition
	] # This one will be used in weigh_matching, raw, thus not neccesary to convert to np array.

def filter_negatives(arr):
	return arr*(arr>0)

"""
Calculates weight difference from matching.
k is the convergence rate
For my implementation k=2 => x**2
k=0.5 => sqrt(x)
For reference k=2 =>-2x
k=0.5 => -0.5*x
"""
def weigh_matching(qs, k=0.2):
	max_q = max(qs)
	if max_q==0:
		max_q=1
	#return np.array([(q/max_q)**k for q in qs], dtype=float) # Custom implementation, factor "delta"
	return np.array([-k*(1-(q/max_q)) for q in qs], dtype=float) # Reference implementation, summand delta
	# Reference implementation requires filtering of negative g's(or something else is implemented incorrectly).
	# For custom implementation the g's dont converge as quickly, this can be solved with filtering.
"""
Implemented according to my interpretation of the circuit on http://www.giclab.com/sequence2.html page 39-42 //freedick
Their output is the superposition given all gated transforms, however that seems less reasonable to me.
A better answer would be the transforms used to find the solution (the position of the found object).
It recurses over each "step" / transformation function
gss is 2d array with [[g_level1_transformparameter1,g_level1_transformparameter2,...],[g_level2_transformparameter1,...]...]
For multiple templates encapsulate in another function which does everything exactly the same only with [the templates] instead of bs
My interpretation is that the templates are the w's in the diagram. Thus if you only use 1 template you feed it directly into L2
"""
def msc_iteration(r, last_superpos, gss, transforms,k=0.2):
	if len(transforms) > 0:
		# Calculate and recurse backward path
		ts,ts_inv = transforms[0]
		bs = [t_inv(last_superpos) for t_inv in ts_inv] # Perform all backward transforms for this step
		gs = gss[0]
		next_gss, prev_fbs = msc_iteration(r, superpos(gs,bs), gss[1:], transforms[1:]) # Perform next step
		prev_f = prev_fbs[0]
		# Calculate and return forward path
		qs = get_matching(prev_f,bs) # Calculate matching
		delta_gs = weigh_matching(qs,k=k) # Calculate the change for weights.
		next_gs = gs + delta_gs # * delta_gs #latter for custom implementation
		next_gs = filter_negatives(next_gs)
		f_transformed = [t(prev_f) for t in ts] # Perform forward transforms
		f_next = superpos(next_gs,f_transformed) # Calculate the superposition of forward transforms
		next_gss= [next_gs]+next_gss # Append the gs for this step to the deeper gss
		f = prev_fbs
	else: # Recursion base case
		f_next = r # When out of transforms, return input as output superposition
		f = []
		next_gss = []
	return next_gss, [f_next]+f+[last_superpos]
def msc_multiple_templates(r, templates, gss, transforms,k=0.2):
	bs = templates # Perform all backward transforms for this step
	gs = gss[0]
	next_gss, f = msc_iteration(r, superpos(gs,bs), gss[1:],
	transforms) # Perform next step
	# Calculate and return forward path
	qs = get_matching_template(f[0],bs) # Calculate matching
	delta_gs = weigh_matching(qs,k=k) # Calculate the change for weights.#There are less templates than transforms atm. K should probably be scaled proportional to the sum of gs.
	next_gs = gs + delta_gs # * delta_gs #latter for custom implementation
	next_gs = filter_negatives(next_gs)
	next_gss= [next_gs]+next_gss # Append the gs for this step to the deeper gss
	
	return next_gss, f
# Test function for webui predef templates
def msc_recog_test_no_template(input_filename,output_filenames,k=0.2, rot_trans_amount = 36*2, x_trans_amount = 73, y_trans_amount = 73):
	template_filenames = ['static/templates/%s_templ.png'%(letter,) for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ"]
	# Load temlate and input from file
	templates = []
	for template_filename in template_filenames:
		template_arr = utils.matrix_from_image(template_filename)
		templates.append(template_arr)
	input_arr = utils.matrix_from_image(input_filename)
	width,height = template_arr.shape
	# Prepare transforms
	rot_params = [360*i/(rot_trans_amount) for i in range(rot_trans_amount)]
	x_params = [width*i/(rot_trans_amount)-(0.5*width) for i in range(x_trans_amount)]
	y_params = [height*i/(rot_trans_amount)-(0.5*height) for i in range(y_trans_amount)]
	raw_rot = lambda arr,ang: (1/255.0)*misc.imrotate(arr,ang, interp='nearest')
	raw_y = lambda arr,dx: np.roll(arr,int(dx), axis=0)
	raw_x = lambda arr,dy: np.roll(arr,int(dy), axis=1)
	rot_trans = [
		TransformContainer(raw_rot,rot_param, "ccw-rotation") for rot_param in rot_params
	]
	inverse_rot_trans = [
		TransformContainer(raw_rot,360-rot_param, "ccw-rotation(inverse)") for rot_param in rot_params
	]
	x_trans = [TransformContainer(raw_x,x_param, "x-translation") for x_param in x_params]
	inverse_x_trans = [TransformContainer(raw_x,-x_param, "x-translation(inverse)") for x_param in x_params]
	y_trans = [TransformContainer(raw_y,y_param, "y-translation") for y_param in y_params]
	inverse_y_trans = [TransformContainer(raw_y,-y_param, "y-translation(inverse)") for y_param in y_params]
	transforms = [
		(rot_trans,inverse_rot_trans),
		(x_trans,inverse_x_trans),
		(y_trans,inverse_y_trans),
	]
	# Set the gating weights so that each initial weight is equal
	gss = [[1/len(transform) for _ in transform] for (transform,_) in transforms]
	gss[0:0] = [[1/len(templates) for _ in templates]]
	# Perform iteration and save superpositions as files for later presentation
	while len(output_filenames)>0:
		gss,imgs = msc_multiple_templates(input_arr, templates, gss, transforms,k)
		imgs.reverse()#Make the templates superpositions come first
		for image in imgs:
			if len(output_filenames)<1:
				break
			utils.image_from_matrix(image).save(output_filenames.pop())
	# Find the most accurate transforms
	template_gs = gss[0]
	best_template = 0
	for i in range(1,len(template_gs)):
		if template_gs[i]>template_gs[best_template]:
			best_template = i
	
	gss = gss[1:]
	resulting_transforms = []
	for j in range(len(gss)):
		gs = gss[j]
		max_index = -1
		for i in range(len(gs)):
			if max_index ==-1 or gs[i]>gs[max_index]:
				max_index = i
		resulting_transforms.append(transforms[j][0][max_index])
	# Generate output string
	output = ""
	for transform in resulting_transforms:
		output+=repr(transform)+" "
	output ="Best template:"+template_filenames[best_template]+output
	return output[:-1]
# Test function for webui
def msc_recog_test(input_filename,template_filename,superpos1_filename,superpos2_filename,k=0.2, rot_trans_amount = 36*2, x_trans_amount = 73, y_trans_amount = 73):
	# Load temlate and input from file
	template_arr = utils.matrix_from_image(template_filename)
	input_arr = utils.matrix_from_image(input_filename)
	width,height = template_arr.shape
	# Prepare transforms
	rot_params = [360*i/(rot_trans_amount) for i in range(rot_trans_amount)]
	x_params = [width*i/(rot_trans_amount)-(0.5*width) for i in range(x_trans_amount)]
	y_params = [height*i/(rot_trans_amount)-(0.5*height) for i in range(y_trans_amount)]
	raw_rot = lambda arr,ang: (1/255.0)*misc.imrotate(arr,ang, interp='nearest')
	raw_x = lambda arr,dx: np.roll(arr,int(dx), axis=0)
	raw_y = lambda arr,dy: np.roll(arr,int(dy), axis=1)
	rot_trans = [
		TransformContainer(raw_rot,rot_param, "ccw-rotation") for rot_param in rot_params
	]
	inverse_rot_trans = [
		TransformContainer(raw_rot,360-rot_param, "ccw-rotation(inverse)") for rot_param in rot_params
	]
	x_trans = [TransformContainer(raw_x,x_param, "x-translation") for x_param in x_params]
	inverse_x_trans = [TransformContainer(raw_x,-x_param, "x-translation(inverse)") for x_param in x_params]
	y_trans = [TransformContainer(raw_y,y_param, "y-translation") for y_param in y_params]
	inverse_y_trans = [TransformContainer(raw_y,-y_param, "y-translation(inverse)") for y_param in y_params]
	transforms = [
		(rot_trans,inverse_rot_trans),
		(x_trans,inverse_x_trans),
		(y_trans,inverse_y_trans),
	]
	# Set the gating weights so that each initial weight is equal
	gss = [[1/len(transform) for _ in transform] for (transform,_) in transforms]
	# Perform an iteration and save superpositions as files for later presentation
	gss,f = msc_iteration(input_arr, template_arr, gss, transforms,k)
	utils.image_from_matrix(f).save(superpos1_filename)
	for i in range(1):
		gss,f = msc_iteration(input_arr, template_arr, gss, transforms,k)
	utils.image_from_matrix(f).save(superpos2_filename)
	# Find the most accurate transforms
	resulting_transforms = []
	for j in range(len(gss)):
		gs = gss[j]
		max_index = -1
		for i in range(len(gs)):
			if max_index ==-1 or gs[i]>gs[max_index]:
				max_index = i
		resulting_transforms.append(transforms[j][0][max_index])
	# Generate output string
	output = ""
	for transform in resulting_transforms:
		output+=repr(transform)+" "
	return output[:-1]

# Test stuff here
if __name__ == "__main__":
	#Create 32 rotation functions (transforms)
	template_arr = np.array([ # Arbitrary template
		[0,0,0,0,1,1],
		[0,0,0,1,1,1],
		[0,0,0,1,0,0],
		[0,0,0,0,0,0],
		[0,0,0,0,0,0],
		[0,0,0,0,0,0],
	], dtype=float)
	input_arr = np.array([ 
		[0,0,0,0,0,0],
		[0,0,0,0,0,0],
		[0,0,0,0,0,0],
		[0,0,0,1,1,0],
		[0,0,0,0,1,1],
		[0,0,0,0,1,1],
	], dtype=float)#template with  -90 rotation
	
	rot_trans_amount = 36*2 #5 degree increment
	rot_params = [360*i/(rot_trans_amount) for i in range(rot_trans_amount)]
	raw_rot = lambda arr,ang: (1/255.0)*misc.imrotate(arr,ang, interp='nearest')
	rot_trans = [
		TransformContainer(raw_rot,rot_param, "ccw-rotation") for rot_param in rot_params
	]
	inverse_rot_trans = [
		TransformContainer(raw_rot,360-rot_param, "ccw-rotation(inverse)") for rot_param in rot_params
	]
	transforms = [(rot_trans,inverse_rot_trans),#,16)
			#(x_trans,inverse_x_trans,40)
			#(y_trans,inverse_y_trans,40)
	]
	in_gss = [[1/len(transform) for _ in transform] for (transform,_) in transforms]
	print("in_gss:")
	print(in_gss)
	out_gss,f = msc_iteration(input_arr, template_arr, in_gss,transforms)
	print("gatings after 1 iteration:")
	print(out_gss)
	out_gss2,f = msc_iteration(input_arr, template_arr, out_gss,transforms)
	print("gatings after 2 iterations:")
	print(out_gss2)
	gss = out_gss2
	for i in range(30):
		gss,f = msc_iteration(input_arr, template_arr, gss,transforms)#[template_arr]
	print("gatings after 32 iterations:")
	print(gss)
	print("resulting superposition f:")
	print(f)
	resulting_transforms = []
	for j in range(len(gss)):
		gs = gss[j]
		max_index = -1
		for i in range(len(gs)):
			if max_index ==-1 or gs[i]>gs[max_index]:
				max_index = i
		resulting_transforms.append(transforms[j][0][max_index])
	for transform in resulting_transforms:
		print("Most prominent transform:",transform)

