from PIL import Image
import numpy, random, string
from scipy import misc

def matrix_from_image(name):
	i = misc.imread(name, flatten=True)
	return numpy.uint8(i<128) #black is 1, white is 0p  !=1

def image_from_matrix(m):
	max_pixel = max([max(x) for x in m])
	if max_pixel == 0:#All black pixels prevent divisian by 0
		return Image.fromarray(numpy.uint8((0*m)), mode="L")
	return Image.fromarray(numpy.uint8(((1.0/max_pixel)*255.0*m)), mode="L")

def flatten_matrix(m):
	return numpy.reshape(m,-1)

def to_matrix(m,side):
	return numpy.reshape(m,(side,side))

def perform_rotations(m,amount):
	original = numpy.copy(m)
	angle = 360/amount
	for i in range(amount):
		m += misc.imrotate(original, i*angle)
	m /= amount
	return m

def makeRandomId(length):
	return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(length))

def save_image(i):
	iid = makeRandomId(6)
	i.save('./static/img/%s.png' % iid)
	return iid
	
def purgeImages(age):
	#TODO: remove all images older than age
	pass

if __name__ == '__main__':
	a = matrix_from_image("centera.png")
	a = perform_rotations(a,4)
	b = image_from_matrix(a)
	b.show()

