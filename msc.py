"""
Exports MSC.
"""

import random
import numpy as np

class MSC:
	"""
	MSC is an implementation of the map-seeking circuits algorithm.
	Objects are initialized with a 2D-array/list of transformations and
	a list of templates.
	Currently, a single template in the form of a numpy array is expected.
	"""

	def __init__(self, xforms, memory):
		"""
		xforms contains transformations (functions).
		Each element is a list of pairs. The list represents one layer, and each
		element in that list represents one transformation. Every transformation
		is paired with its inverse.
		For example
		xforms = [[(t11,t11'), (t12,t12')], [(t21,t21'), (t22,t22')]]
		has two layers, and there are two possible transformations at each layer.
		"""
		self.memory = memory
		self.forwards = [
			[t for (t,_) in ts] for ts in xforms
		]
		self.backwards = [
			[it for (_,it) in its] for its in xforms
		]
		self.backwards.reverse()

	# Should probably return the matching template as well
	# (in addition to transformations).
	def recall(self, r, qf=None, ks=None, gthresh=0):
		"""
		Try to find a match for input r.
		Returns a list of transformations - one for each "layer".
		"""
		fwd = self.forwards
		bwd = self.backwards
		gss = init_gss(self.forwards)
		bL = self.memory[0] # w*1 (assume a single template for now)
		while not converged(gss):
			gss = process_iteration(r, gss, fwd, bwd, bL, ks=ks, gthresh=gthresh)
		return get_solution(gss)


def init_gss(xforms):
	return [np.ones(len(l), np.float) for l in xforms]

# Do one full iteration. Returns the new weights (g-values, gss).
# itss are the inverse tss.
def process_iteration(img, gss, tss, itss, template, ks=None, gthresh=0):
	fs = forward_path(img, gss, tss)
	bs = backward_path(template, gss, itss)
	qss = calculate_matches(fs, bs) # Pass in as parameter.
	return g_update(gss, qss, ks=ks, gthresh=gthresh)

def forward_path(img, gss, tss):
	return [tr for (_,tr) in build_path(img, gss, tss)[:-1]]

def backward_path(img, gss, tss):
	bs = [bl for (bl,_) in build_path(img, gss, tss)[:-1]]
	bs.reverse()
	return bs

# Returns qss.
def calculate_matches(fss, bs):
	return [np.array([np.dot(f,b) for f in fs]) for (fs,b) in zip(fss, bs)]

# [(f^l, [tj(f^l)])]
# Also works for b. (At least it's supposed to.)
def build_path(img, gss, tss):
	if len(gss) == 0:
		return [(img,[])]
	gs = gss[0]
	ts = tss[0]
	tr = xforms(img, ts)
	sup = superpos(gs, tr)
	path = build_path(sup, gss[1:], tss[1:])
	path.insert(0,(img,tr))
	return path

# All the transformations (ts) applied to img.
def xforms(img, ts):
	return [t(img) for t in ts]

# Superposition of all transformed images (tr), weighted by gs.
def superpos(gs, tr):
	return sum([g*u for (g,u) in zip(gs, tr)])

# Could take a competition function.
def g_update(gss, qss, ks=None, gthresh=0):
	threshold = lambda x: x if x > gthresh else 0
	ks = ks or [1]*len(gss)
	return np.array([
		list(map(threshold, [g-k*(1-q/max(qs)) for (g,q) in zip(gs,qs)]))
			for (gs,qs,k) in zip(gss,qss,ks)
	])

# Checks whether there is exactly one non-zero value in each layer.
def converged(gss):
	return all([len([g for g in gs if g > 0]) == 1 for gs in gss])

def get_solution(gss):
  tmp = [[i for i in range(len(gs)) if gs[i] > 0] for gs in gss]
  return [l[0] for l in tmp]


# Form the next forward superposition f^l at layer l by applying each
# available transform t_j to f^(l-1), multiplying by the weight g for
# that t, and adding all these products together.
#
# fl = sum(glj*tlj(fl-1))

# Form the backward superposition at layer l in the same way, except that
# we are moving in the opposite direction. So the "previous" layer is
# l+1 instead of l-1.
#
# bl = sum(glj*tlj(bl+1))

# Calculate the degree of match for transform i at layer l using the
# dot-product of the transformation and the backward path from the next
# layer.
#
# qli = tli(fl-1) * bl+1

# Use q-values to update g-values.
# gli -= -k(max(ql) - qli)

# Note: you have to keep track of sub- and superscripts while parsing
# these expressions. Writing them out unambiguously using something like
# LaTeX notation here would make them extremely verbose.
# For example, "fl-1" is the forward superposition f, labeled with the
# superscript "l-1" to indicate the layer.
