$(function(){ $("#debug").hide(); });

$("#hidebtn").click(function(){$("#debug").toggle()});

$("form").submit(function(e){
	e.preventDefault()
	var postData = $(this).serialize();
	$.ajax({
		url: "/recognize",
		type: "POST",
		data: postData,
		success: function(r){
			var resp = $.parseJSON(r)
			$("#image").attr("src","/static/img/"+resp.imageid);
		}
	});
});