$("input[name='resx']").change(function(){
	var v = $(this).val();
	if($("input[name='resy']").prop("disabled")) $("input[name='resy']").val(v)
	$("input[name='transx']").attr("max", v);
	$("input[name='transy']").attr("max", v);
});

$("input[name='square']").change(function(){
	if($(this).is(":checked")){ $("input[name='resy']").prop("disabled", true);
	} else { $("input[name='resy']").prop("disabled", false);	}
});

$("input[name='rot']").change(function(){
	var deg = parseInt($(this).val())
	if(deg>359){ $(this).val(deg-360);
	}else if (deg<0) { $(this).val(deg+360); };
});

$("form").submit(function(e){
	e.preventDefault()
	$("#thebutton").prop("disabled", true);
	$("#spincont").show();
	$("#image").addClass("loading");
	$("input[name='resy']").prop("disabled", false); // isn't included otherwise
	var postData = $(this).serialize();
	$("input[name='resy']").prop("disabled", true);
	$.ajax({
		url: "/generate",
		type: "POST",
		data: postData,
		success: function(r){
			var resp = $.parseJSON(r)
			$("#thebutton").prop("disabled", false);
			$("#spincont").hide();
			$("#image").removeClass("loading");
			$("#image").attr("src","/static/img/"+resp.imageid);
		}
	});
});

function randInt(min, max) { return Math.floor(Math.random() * (max - min + 1)) + min; }
function randASCII() { return String.fromCharCode(randInt(33,126)); }

$("#randomize").click(function(){
	var maxx = $("input[name='resx']").val();
	var maxy = $("input[name='resy']").val();
	if($("#tlock").is(":checked")){
		$("input[name='transx']").val(randInt(0, maxx));
		$("input[name='transy']").val(randInt(0, maxy));
	}
	if($("#rlock").is(":checked")) $("input[name='rot']").val(randInt(0, 360));
	if($("#glock").is(":checked")) $("input[name='glyph']").val(randASCII());
});

$(function(){ $("#spincont").hide(); });