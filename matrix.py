#TODO: Invert to conform with MSC/Evgeni model. => 1-(inpImg.getpixel((x,y))/255.0)
#(Black pixel 0 or 1?)
from PIL import Image
def imgToMatrix(inpImg):
	inpImg.convert('L')#Convert to grayscale
	(width,height)= inpImg.size
	outMatrix = [[0.0]*height]*width #Accessed with [x][y]
	for x in range(width):
		for y in range(height):
			outMatrix[x][y]=inpImg.getpixel((x,y))/255.0
	return outMatrix

#TODO:Make sure matrices are same size
def superpositionMatrix(matrices):
	superposition = [[0.0]*len(matrices[0][0])]*len(matrices[0])
	for matrix in matrices:
		for x in range(len(matrix)):
			for y in range(len(matrix)):
				superposition[x][y]+=matrix[x][y]
	return superposition

#Mainly intended for debugging and presentation
def matrixToImg(inpMatrix):
	(width,height)=(len(inpMatrix),len(inpMatrix[0]))
	out = Image.new('L',(width, height),'black')
	maxVal = max([max(n) for n in inpMatrix])#Find max value in 2d array/matrix
	for x in range(width):
		for y in range(height):
			out.setpixel(x,y)=(inpMatrix[x][y]/maxVal)*255
