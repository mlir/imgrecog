from PIL import Image
def transformRotate(inpImg, degrees, do_resample=False):
	out = inpImg.rotate(degrees, resample = Image.BICUBIC if do_resample else Image.NEAREST)
	return out
def transformTranslateY(inpImg, topY, height):
	(width, _) = inpImg.size
	out = Image.new("L", (width,height), "white")
	out.paste(inpImg,(0,topY))
	return out
def transformTranslateX(inpImg, leftX, width):
	(_, height) = inpImg.size
	out = Image.new("L", (width,height), "white")
	out.paste(inpImg,(leftX,0))
	return out
